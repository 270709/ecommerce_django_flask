from datetime import date
from django.contrib import messages
from prodotti.forms import *
from .models import *
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from .models import *

def listaprodotti(request):
    catalogo = Prodotto.objects.order_by('-media_valutazione')
    ctx = {'catalogo' : catalogo}
    return render(request,'prodotti/catalogo.html',ctx)

def agg_carrello(request,id):
    form = AggiuntaCarrelloForm()
    prod = Prodotto.objects.get(id_prodotto=id)
    if request.method == 'POST':
        form = AggiuntaCarrelloForm(request.POST)
        prod = Prodotto.objects.get(pk=id)
        us_pren = User.objects.get(pk=request.user.id)
        if form.is_valid():
            c = form.save(commit=False)
            if c.qta_richiesta > prod.qta_disponibile:
                messages.warning(request,"Quantità non disponibile")
            else:
                c.user = us_pren
                c.prodotto = prod
                prod.qta_disponibile -= c.qta_richiesta
                prod.save()
                c.save()
                messages.success(request,"Aggiunto al carrello con successo")
                return redirect('home')
    ctx = {'prodotto' : prod, 'form' : form, 'catalogo' : listaprodotti }
    return render(request, 'prodotti/carrello.html',ctx)

def prenotazione(request):
    id=request.user.id
    carrello=[]
    car=Carrello.objects.all()
    costo=0
    ap=0
    for c in car:
        if c.user_id == id:
            carrello.append(c)
            ap= c.prodotto.prezzo * c.qta_richiesta
            costo += ap

    ctx={'carrello' : carrello,'totale' : costo}
    return render(request,'prodotti/prenotazione.html',ctx)

def acquisto(request):
    car = Carrello.objects.all()
    car_user=[]
    for c in car:
        if c.user_id == request.user.id:
            car_user.append(c)
        dele = Carrello.objects.get(pk=c.id)
        dele.delete()
    today=date.today()
    for a in car_user:
        acquisto = Storico_Ac(user_id=a.user_id,prodotti=a.prodotto,qta=a.qta_richiesta,data=today)
        acquisto.save()
    messages.success(request,"Ordine effettuato")
    return redirect('home')

def storico(request):
    acquisti = Storico_Ac.objects.all()
    ac_user = []
    for c in acquisti:
        if c.user_id == request.user.id:
            ac_user.append(c)
    ctx={'storico' : ac_user}
    return render(request,'prodotti/storico.html',ctx)

def recensione(request,id):
    form = RecensioneForm()
    S_prodotto = Prodotto.objects.get(pk=id)
    user = User.objects.get(pk=request.user.id)

    if request.method == 'POST':
        form = RecensioneForm(request.POST)
        if form.is_valid():
            v = form.save(commit=False)
            v.user = user
            v.prodotto = S_prodotto
            v.save()

            storico = Valutazione.objects.filter(prodotto=S_prodotto)
            somma = 0
            somma += v.voto
            for s in storico:
                somma += s.voto
            media_valutazione = somma / (len(storico) + 1)
            S_prodotto.media_valutazione = round(media_valutazione,1)
            S_prodotto.save()

            messages.success(request,"Recensione prodotto avvenuta correttamente")
            return redirect('home')
    ctx = { 'form': form }
    return render(request,'prodotti/votazione.html',ctx)

def modificaOrdine(request,id):
    form = ModificaForm()
    if request.method == 'POST':
        form = ModificaForm(request.POST)
        if form.is_valid():
            v = form.save(commit=False)
            car = Carrello.objects.get(pk=id)
            if request.user.id == car.user.id:
                prod = Prodotto.objects.get(pk=car.prodotto.id_prodotto)
                qta = v.qta_richiesta - car.qta_richiesta
                if prod.qta_disponibile < qta:
                    messages.error(request,"errore quantità prodotto non sufficiente")
                else:
                    ap=car.qta_richiesta
                    car.qta_richiesta=v.qta_richiesta
                    ap = v.qta_richiesta - ap
                    prod.qta_disponibile-=ap
                    v.user = request.user
                    v.prodotto= prod
                    car.delete()
                    prod.save()
                    v.save()
                    messages.success(request,"modifica avvenuta con successo")
                    return redirect('home')
        else:
            messages.error(request,"errore nel form")
    ctx = { 'form': form }
    return render(request,'prodotti/modifica.html',ctx)

def cancellaOrdine(request,id):
    car = Carrello.objects.get(pk=id)
    if request.user.id == car.user.id:
        qta = car.qta_richiesta
        prod = Prodotto.objects.get(pk=car.prodotto.id_prodotto)
        prod.qta_disponibile += qta
        prod.save()
        car.delete()
        messages.success(request,"cancellazione effetuata con successo")
    return redirect('home')


