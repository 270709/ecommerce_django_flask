from django.urls import reverse
from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.

class LoginTestVieW(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = {
            'username' : 'user1',
            'email' : 'user@prova.com',
            'password' : '12345'
        }
        User.objects.create_user(**self.user1)
        self.login_url = reverse('login')


    def test_login_corretto(self):
        response = self.client.post(self.login_url,self.user1, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)

    def test_login_password_errata(self):
        response = self.client.post(self.login_url,{
            'username' : 'user1',
            'password' : '444444'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_username_errato(self):
        response = self.client.post(self.login_url,{
            'username' : 'user2',
            'password' : '12345'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_username_e_password_errato(self):
        response = self.client.post(self.login_url,{
            'username' : 'user2',
            'password' : 'provapassword'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)  


    
