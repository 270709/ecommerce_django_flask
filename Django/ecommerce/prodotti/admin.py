from django.contrib import admin

# Register your models here.

from prodotti.models import *

admin.site.register(Prodotto)
admin.site.register(Carrello)
admin.site.register(Storico_Ac)
admin.site.register(Valutazione)