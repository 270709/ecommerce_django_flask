from django.shortcuts import render
from django.urls import reverse_lazy
from .forms import *
from django.views.generic.edit import CreateView
from django.contrib import messages
from functools import wraps
import time

def timer(func):
    """funzione per stimare la velocità di esecuzione"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        # start time
        start = time.time()
        # func execution
        result = func(*args, **kwargs)
        
        duration = (time.time() - start) * 1000
        # output su console
        print('view {} takes {:.2f} ms'.format(
            func.__name__, 
            duration
            ))
        return result
    return wrapper

def ecommerce_home(request):
    return render(request,"home.html")

class UserCreate(CreateView):
    form_class = CreaUtentelog
    template_name="create_user.html"
    success_url = reverse_lazy('login')

    def form_valid(self, form):
      messages.success(self.request, "utente creato correttamente")
      return super().form_valid(form)

def AboutUs(request):
    return render(request,'about.html')


