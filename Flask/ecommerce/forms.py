
from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField,DecimalField,IntegerField,SelectField
from flask_wtf.file import FileField, FileAllowed,FileRequired
from wtforms.validators import DataRequired,Length,EqualTo,Email,NumberRange

class RegistrationForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired(),Length(min=3,max=20)])
    email = StringField('Email',validators=[DataRequired(),Email()])
    name = StringField('Nome',validators=[DataRequired(),Length(min=3,max=20)])
    surname = StringField('Cognome',validators=[DataRequired(),Length(min=3,max=20)])
    password = PasswordField('Password',validators=[DataRequired(),Length(min=6,max=40)])
    password_confirm = PasswordField('Conferma password',validators=[DataRequired(),EqualTo('password')])
    submit = SubmitField('Registrati ora')

class LoginForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired(),Length(min=3,max=20)])
    password = PasswordField('Password',validators=[DataRequired(),Length(min=6,max=40)])
    submit = SubmitField('Login')

class ProdottoForm(FlaskForm):
    nome = StringField('Nome prodotto',validators=[DataRequired(),Length(min=1,max=50)])
    immagine = FileField('immagine prodotto', validators=[FileAllowed(['jpg', 'png','jpeg'], 'Images only!')])
    prezzo = DecimalField('prezzo',places=2,validators=[DataRequired()])
    categoria = SelectField('categoria',choices=[('Frutta','Frutta'),('Verdura','Verdura'),('Bulbi','Bulbi'),('Animali','Animali'),('Fitosanitari','Fitosanitari')],validators=[DataRequired()])
    qta_disponibile = IntegerField('quantità disponibile',validators=[DataRequired()])
    media_valutazione = IntegerField('media valutazione',validators=[DataRequired()])
    descrizione = StringField('Descrizione',validators=[DataRequired(),Length(min=1,max=300)])
    submit = SubmitField('Aggiungi prodotto')

class CarrelloForm(FlaskForm):
    qta_richiesta = IntegerField('quantità richiesta',validators=[DataRequired(),NumberRange(min=1,max=50)])
    submit = SubmitField("Aggiungi al carrello")

class RecensioniForm(FlaskForm):
    voto = IntegerField('valutazione',validators=[DataRequired(),NumberRange(min=1, max=10, message="Aggiungi una valutazione")])
    submit = SubmitField('Aggiungi recensione')

class ModificaForm(FlaskForm):
    qta_modifica = IntegerField('quantità aggiornata',validators=[DataRequired(),NumberRange(min=1,max=50)])
    submit = SubmitField('Modifica ordine')