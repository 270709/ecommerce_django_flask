import enum
from flask_appbuilder.models.mixins import ImageColumn
from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

class Categoria(enum.Enum):
    Frutta = 'Frutta'
    Verdura = 'Verdura'
    Bulbi = 'Bulbi'
    Animali = 'Animali'
    Fitosanitari = 'Fitosanitari'

class User(db.Model,UserMixin):

    __tablename__ = 'User'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20),unique=True)
    name = db.Column(db.String(20))
    surname = db.Column(db.String(20))
    email = db.Column(db.String(70))
    password_hash = db.Column(db.String(300))
    authenticated = db.Column(db.Boolean,default=False)
    carrello = db.relationship('Carrello',backref='user')
    storico = db.relationship('Storico',backref='user')
    recensione = db.relationship('Recensioni',backref='user')

    @property
    def password(self):
        raise AttributeError("password non è un oggetto readble")
    @password.setter
    def password(self,password):
        self.password_hash=generate_password_hash(password)
    
    def verify_password(self,password):
        return check_password_hash(self.password_hash,password)

    def __repr__(self):
	    return self.name

class Prodotto(db.Model):
    __tablename__ = 'Prodotto'

    id_prodotto = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(50))
    immagine = db.Column(ImageColumn(size=(300, 300, True), thumbnail_size=(30, 30, True)))
    prezzo = db.Column(db.Numeric(precision=10,scale=2),nullable=False)
    categoria = db.Column(db.Enum(Categoria),default=Categoria.Frutta,nullable=False)
    qta_disponibile = db.Column(db.Integer(),default=0)
    media_valutazione= db.Column(db.Integer(),default=1)
    descrizione = db.Column(db.String(300))
    carrello = db.relationship('Carrello',backref='prodotto')
    storico = db.relationship('Storico',backref='prodotto')
    recensione = db.relationship('Recensioni',backref='prodotto')

    def __str__(self):
        return self.nome

class Carrello(db.Model):
    __tablename__ = 'Carrello'

    id_carrello = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('User.id'))
    prodotto_id = db.Column(db.Integer,db.ForeignKey('Prodotto.id_prodotto'))
    qta_richiesta = db.Column(db.Integer(),default=0)

    def __str__(self):
        return str(self.user_id) + " " + str(self.prodotto_id)

class Storico(db.Model):
    __tablename__ = 'Storico'

    id_storico = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('User.id'))
    prodotto_id = db.Column(db.Integer,db.ForeignKey('Prodotto.id_prodotto'))
    qta = db.Column(db.Integer(),default=0)
    data = db.Column(db.DateTime)

    def __str__(self):
        return str(self.user_id) + "in data " + str(self.data) + " " + "ha acquistato " + str(self.prodotto_id)

class Recensioni(db.Model):
    __tablename__ = 'Recensioni'

    id_recesione = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('User.id'))
    prodotto_id = db.Column(db.Integer,db.ForeignKey('Prodotto.id_prodotto'))
    voto = db.Column(db.Integer(),default=1)

    def __str__(self):
        return str(self.prodotto_id) + "Voto: " + str(self.voto)