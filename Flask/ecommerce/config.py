import os

BASE_DIR = os.path.dirname(os.path.abspath(__name__))
SECRET_KEY = os.urandom(32)

class Config:
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'ecommerce.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.urandom(32)