import unittest
from app import app, db
from models import User

class TestLogin(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        db.create_all()

        # creazione utente di prova
        user = User(username='prova', name='testuser', surname='usertest', email='test.user@example.com')
        user.password = 'password'
        db.session.add(user)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_login_successo(self):
        with app.test_client() as client:
            response = client.post('/login/', data=dict(
                username='prova',
                password='password'
            ), follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            self.assertIn(b'Login effetuato come  testuser, Benvenuto', response.data)

    def test_invalid_password(self):
        with app.test_client() as client:
            response =client.post('/login/', data=dict(
            username='prova',
            password='wrongpassword'
        ), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'wrong password - try again!!', response.data)

    def test_invalid_username(self):
        with app.test_client() as client:
            response =client.post('/login/', data=dict(
            username='wrongusername',
            password='password'
        ), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'user doesnt exist - try again!!', response.data)

    def test_invalid_username_and_password(self):
        with app.test_client() as client:
            response =client.post('/login/', data=dict(
            username='wrongusername',
            password='wrongpassword'
        ), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'user doesnt exist - try again!!', response.data)

if __name__ == '__main__':
    unittest.main()